﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance
    {
        get
        {
            if (_instance is null)
            {
                Debug.LogError("Audio Manager is missing");

                return null;
            }

            return _instance;
        }
    }

    private static AudioManager _instance;
    [SerializeField] private AudioSource sfxSource = null;
    [SerializeField] private AudioSource bgmSource = null;

    public void TriggerSfx(AudioClip clip)
    {
        sfxSource.PlayOneShot(clip);
    }

    public void TriggerSfx(AudioClip clip, float volume)
    {
        sfxSource.PlayOneShot(clip, volume);
    }

    public void PlayBGMLoop(AudioClip clip)
    {
        bgmSource.clip = clip;
        bgmSource.loop = true;
        if(bgmSource.clip != null)
        {
            bgmSource.Play();
        }
    }

    private void Awake()
    {
        _instance = this;
    }
}
