﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] clips;
    private void Step()
    {
        AudioClip clip = clips[UnityEngine.Random.Range(0, clips.Length)];
        AudioManager.Instance.TriggerSfx(clip, .3f);
    }
}
