﻿using UnityEngine;

public class EnemyRaycast : MonoBehaviour
{
    [SerializeField]
    private EnemyMovement enemyMovement;
    [SerializeField]
    private LayerMask layerMask;
    [SerializeField]
    private Transform raycastTransform;
    [SerializeField]
    private float maxDistance = 5;
    [SerializeField]
    private float circleCastSize = 1;
    [SerializeField]
    private AudioClip clipWhenFoundPlayer = null;

    private bool _isHit = false;
    private float _distance = 0f;
    private bool _isPlayerFound = false;

    private void Update()
    {
        FireRaycast();
    }

    private void FireRaycast()
    {
        if (_isPlayerFound)
        {
            return;
        }

        RaycastHit hit;

        _isHit = Physics.SphereCast(raycastTransform.position, circleCastSize, transform.forward, out hit, maxDistance, layerMask);

        if (_isHit)
        {
            _distance = hit.distance;

            if (hit.transform.TryGetComponent(out Player player))
            {
                enemyMovement.PlayerFound();
                player.Die();
                _isPlayerFound = true;
                AudioManager.Instance.TriggerSfx(clipWhenFoundPlayer);
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (_isHit)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(raycastTransform.position, transform.forward * _distance);
            Gizmos.DrawWireSphere(raycastTransform.position + transform.forward * _distance, circleCastSize);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(raycastTransform.position, transform.forward * maxDistance);
        }
    }
}
