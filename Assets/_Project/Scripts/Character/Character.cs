﻿using System;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public CharacterData characterData;

    public abstract void Die();
}

[Serializable]
public struct CharacterData
{
    private const float MinHealth = 0;

    [SerializeField] private int _currentHealth;
    [SerializeField] private int _maxHealth;
    [SerializeField] private float _speed;

    public int MaxHealth
    {
        get => _maxHealth;
        set => _maxHealth = value;
    }

    public int CurrentHealth
    {
        get => _currentHealth;
        set => _currentHealth = value;
    }

    public float Speed
    {
        get => _speed;
        set => _speed = value;
    }
}
