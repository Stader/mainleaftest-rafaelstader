﻿using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    private static readonly int Walking = Animator.StringToHash("walking");
    private Animator _animator;
    private static readonly int PlayerFound = Animator.StringToHash("playerFound");

    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        _animator = GetComponent<Animator>();
    }

    public void SetAnimations(bool walking, bool playerFound = false)
    {
        _animator.SetBool(Walking, walking);
        _animator.SetBool(PlayerFound, playerFound);
    }
}
