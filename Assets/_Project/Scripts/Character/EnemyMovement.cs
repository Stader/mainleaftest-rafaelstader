﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public sealed class EnemyMovement : MonoBehaviour, IPauseObject
{
    [SerializeField]
    private NavMeshAgent agent = null;
    [SerializeField]
    private Enemy enemy = null;
    [SerializeField]
    private EnemyAnimation _animation = null;
    [SerializeField]
    private Transform[] pathPoints;
    [SerializeField]
    private float idleTime = 3;

    private int _currentPathIndex = -1;
    private bool _reachPath = false;
    private bool _canGenerateOtherPath = false;
    private bool _canMove = true;
    private Coroutine _coroutine = null;

    public void PlayerFound()
    {
        _canMove = false;
        _animation.SetAnimations(false, true);
    }

    private void Start()
    {
        NextPath();
    }

    private void Update()
    {
        Move();
    }

    private void OnEnable()
    {
        PauseMenu.Pause += Pause;
        PauseMenu.UnPause += UnPause;
    }

    private void OnDisable()
    {
        PauseMenu.Pause -= Pause;
        PauseMenu.UnPause -= UnPause;
    }

    private void Move()
    {
        if (!_canMove)
        {
            agent.speed = 0;
            return;
        }

        if (agent.speed != enemy.characterData.Speed)
        {
            agent.speed = enemy.characterData.Speed;
        }

        if (!agent.hasPath && _reachPath)
        {
            _reachPath = false;
            _canGenerateOtherPath = true;
        }

        if (_canGenerateOtherPath && !_reachPath)
        {
            _coroutine = StartCoroutine(StartPath());
        }
    }

    private void NextPath()
    {
        _currentPathIndex++;
        if (_currentPathIndex >= pathPoints.Length)
        {
            _currentPathIndex = 0;
        }
        agent.SetDestination(pathPoints[_currentPathIndex].localPosition);
        _reachPath = true;
        _canGenerateOtherPath = true;

        if (!_canMove)
        {
            return;
        }
        _animation.SetAnimations(true);
    }

    private IEnumerator StartPath()
    {
        _canGenerateOtherPath = false;
        _animation.SetAnimations(false);

        yield return new WaitForSeconds(idleTime);

        NextPath();
    }

    public void Pause()
    {
        _canMove = false;
        _animation.SetAnimations(false);
    }

    public void UnPause()
    {
        _canMove = true;

        if (agent.hasPath)
        {
            _animation.SetAnimations(true);
        }
    }
}
