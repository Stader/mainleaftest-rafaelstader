﻿using Cinemachine;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

[SelectionBase]
public sealed class Player : Character
{
    public bool Died => _died;

    private bool _died = false;

    public void Initialize(int maxHealth, float speed, bool startWithFullHealth = true)
    {
        characterData.MaxHealth = maxHealth;

        characterData.CurrentHealth = startWithFullHealth ? maxHealth : 1;

        characterData.Speed = speed;
    }

    public override void Die()
    {
        _died = true;
        CinemachineFreeLook freeLookCamera = FindObjectOfType<CinemachineFreeLook>();

        if (freeLookCamera != null)
        {
            freeLookCamera.LookAt = null;
            freeLookCamera.Follow = null;
        }

        DieAsync();
    }

    private static async void DieAsync()
    {
        LevelManager.Instance.BeforeChangeScene();

        await Task.Delay(1000);

        SceneLoader.Instance.LoadNewScene("Level1");
    }
}
