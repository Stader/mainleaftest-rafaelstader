﻿using UnityEngine;

public class PlayerPullDetect : MonoBehaviour
{
    [SerializeField] private PlayerPhysics playerPhysics = null;

    private void OnTriggerEnter(Collider other)
    {
        PullObject pullObject = other.GetComponent<PullObject>();

        if (pullObject == null)
        {
            return;
        }

        playerPhysics.CanHook = true;
        playerPhysics.hookObjectReference = other.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        PullObject pullObject = other.GetComponent<PullObject>();

        if (pullObject == null)
        {
            return;
        }

        playerPhysics.CanHook = false;
    }
}
