﻿using UnityEngine;
using UnityEngine.InputSystem;

public sealed class PlayerInput : MonoBehaviour
{
    public bool gamePaused = false;

    [SerializeField] private PlayerPhysics playerPhysics = null;

    private PlayerControls _playerControls;

    private void OnMove(InputValue value)
    {
        Vector2 direction = value.Get<Vector2>();

        playerPhysics.Direction = direction;
    }

    private void OnJump()
    {
        playerPhysics.Jump();
    }

    private void OnCrouch()
    {
        playerPhysics.Crouch();
    }

    private void OnCrouchUp()
    {
        playerPhysics.Crouch(false);
    }

    private void OnCrouchDown()
    {
        playerPhysics.Crouch(true);
    }

    private void OnAction()
    {
        playerPhysics.Action();
    }

    private void OnPause()
    {
        if (gamePaused)
        {
            PauseMenu.UnPause?.Invoke();
        }
        else
        {
            PauseMenu.Pause?.Invoke();
        }
    }
}
