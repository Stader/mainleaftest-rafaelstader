﻿public sealed class Enemy : Character
{
    public void Initialize(int maxHealth, float speed, bool startWithFullHealth = true)
    {
        characterData.MaxHealth = maxHealth;

        characterData.CurrentHealth = startWithFullHealth ? maxHealth : 1;

        characterData.Speed = speed;
    }

    public override void Die()
    {
    }
}
