﻿using System;
using UnityEngine;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(CharacterController))]
public sealed class PlayerPhysics : MonoBehaviour, IPauseObject
{
    [HideInInspector] public GameObject hookObjectReference;

    public static event Action OnCanHook;
    public static event Action OnRemoveCanHook;
    public bool IsGrounded => _isGrounded;

    public Vector2 Direction
    {
        get => direction;
        set => direction = value;
    }

    public bool CanHook
    {
        get => _canHook;
        set
        {
            _canHook = value;
            if (value)
            {
                OnCanHook?.Invoke();
            }
            else
            {
                OnRemoveCanHook?.Invoke();
            }
        }
    }

    [SerializeField] private Player player = null;
    [SerializeField] private CharacterController characterController = null;
    [SerializeField] private PlayerAnimation playerAnimation = null;
    [SerializeField] private Transform camPlayer = null;
    [SerializeField] private Transform groundCheck = null;
    [SerializeField] private Vector2 direction;
    [SerializeField] private float turnSmoothStep = 0.1f;
    [SerializeField] private float jumpForce = 5;
    [SerializeField] private float fallSpeed = 1;
    [SerializeField] private LayerMask groundMask;

    private const float Gravity = -9.81f;
    private float _fallVelocity;
    private float _turnSmoothSpeed;
    private bool _isGrounded;
    private bool _isCrouched;
    private bool _isHooked;
    private bool _canHook;
    private bool _canMove = true;
    private float _reduceSpeedMultiplier = 1;

    public void Jump()
    {
        if (_isGrounded && !_isHooked)
        {
            _fallVelocity = Mathf.Sqrt(jumpForce * -Gravity);
        }
    }

    public void Crouch()
    {
        if (_isCrouched)
        {
            characterController.height = 2f;
            characterController.center = new Vector3(0f,1f,0f);
            _isCrouched = false;
            _reduceSpeedMultiplier = 1;
        }
        else
        {
            characterController.height = 1.4f;
            characterController.center = new Vector3(0f,.7f,0f);
            _isCrouched = true;
            _reduceSpeedMultiplier = 0.5f;
        }
    }

    public void Crouch(bool isCrouch)
    {
        _isCrouched = isCrouch;
        if (_isCrouched)
        {
            characterController.height = 1.2f;
            characterController.center = new Vector3(0f,.6f,0f);
            _reduceSpeedMultiplier = 0.5f;
        }
        else
        {
            characterController.height = 2f;
            characterController.center = new Vector3(0f,1f,0f);
            _reduceSpeedMultiplier = 1;
        }
    }

    public void Action()
    {
        if (_canHook)
        {
            if (!_isHooked)
            {
                hookObjectReference.GetComponent<PullObject>().OnHook(transform);
                _isHooked = true;
                _reduceSpeedMultiplier = 0.5f;
            }
            else
            {
                BreakHook();
            }
        }
        else if (_isHooked)
        {
            BreakHook();
        }
    }

    private void Start()
    {
        Initialize();
    }

    private void Update()
    {
        Move();
        CheckGround();
        Animate();
    }

    private void OnEnable()
    {
        PauseMenu.Pause += Pause;
        PauseMenu.UnPause += UnPause;
    }

    private void OnDisable()
    {
        PauseMenu.Pause -= Pause;
        PauseMenu.UnPause -= UnPause;
    }

    private void Initialize()
    {
        if (camPlayer == null)
        {
            camPlayer = Camera.main?.transform;
        }
    }

    private void Move()
    {
        Vector3 newDir = new Vector3(direction.x, 0, direction.y);

        ApplyGravity();

        if (player.Died || !_canMove)
        {
            return;
        }

        if (newDir.magnitude > 0.1f)
        {
            float turnAngle = Mathf.Atan2(newDir.x, newDir.z) * Mathf.Rad2Deg + camPlayer.eulerAngles.y;
            float smoothAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, turnAngle, ref _turnSmoothSpeed, turnSmoothStep);

            Vector3 forwardMoveDir = Quaternion.Euler(0f, turnAngle, 0f) * Vector3.forward;

            if(!_isHooked)
            {
                transform.rotation = Quaternion.Euler(0f, smoothAngle, 0f);
            }
            else
            {
                turnAngle = Mathf.Atan2(newDir.x, newDir.z) * Mathf.Rad2Deg + Mathf.Round(transform.eulerAngles.y / 90) * 90;
                forwardMoveDir = Quaternion.Euler(0f, turnAngle, 0f) * Vector3.forward;
            }

            characterController.Move(forwardMoveDir.normalized * player.characterData.Speed * _reduceSpeedMultiplier * Time.deltaTime);
        }
    }

    private void Animate()
    {
        if (player.Died || !_canMove)
        {
            return;
        }

        playerAnimation.SetAnimations(direction, _isCrouched, _isGrounded, _isHooked);
    }

    private void CheckGround()
    {
        _isGrounded = Physics.CheckSphere(groundCheck.position, .2f, groundMask);

        if (_isGrounded && _fallVelocity < 0)
        {
            _fallVelocity = 0f;
        }
    }

    private void ApplyGravity()
    {
        _fallVelocity += Gravity * fallSpeed * Time.deltaTime;

        if (_fallVelocity < -30)
        {
            _fallVelocity = -30;
        }

        Vector3 moveDir = new Vector3(0f, _fallVelocity, 0);

        characterController.Move(moveDir * Time.deltaTime);
    }

    private void BreakHook()
    {
        hookObjectReference.GetComponent<PullObject>().RemoveHook();
        _isHooked = false;
        _reduceSpeedMultiplier = 1f;
    }

    public void Pause()
    {
        _canMove = false;
    }

    public void UnPause()
    {
        _canMove = true;
    }
}
