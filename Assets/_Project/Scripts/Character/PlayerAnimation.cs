﻿using UnityEngine;

public sealed class PlayerAnimation : MonoBehaviour
{
    [SerializeField] private Animator animator = null;

    private static readonly int Jumping = Animator.StringToHash("jumping");
    private static readonly int Walking = Animator.StringToHash("walking");
    private static readonly int Pulling = Animator.StringToHash("pulling");
    private static readonly int Crouching = Animator.StringToHash("crouching");

    public void SetAnimations(Vector2 direction, bool isCrouch, bool isGrounded, bool isHooked)
    {
        if (direction.magnitude > 0.1f)
        {
            SetWalk(true);
        }
        else
        {
            SetWalk(false);
        }

        SetCrouch(isCrouch);
        SetJump(!isGrounded);
        SetHook(isHooked);
    }

    private void SetWalk(bool onWalk)
    {
        animator.SetBool(Walking, onWalk);
    }

    private void SetJump(bool onGround)
    {
        animator.SetBool(Jumping, onGround);
    }

    private void SetHook(bool onHook)
    {
        animator.SetBool(Pulling, onHook);
    }

    private void SetCrouch(bool onCrouch)
    {
        animator.SetBool(Crouching, onCrouch);
    }
}
