﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance
    {
        get
        {
            if (_instance is null)
            {
                Debug.LogError("Game Manager is missing");

                return null;
            }

            return _instance;
        }
    }

    public int Score { get => score; set => score = value; }

    [SerializeField]
    private List<LevelData> levelsData;
    [SerializeField]
    private int score;

    private static GameManager _instance;

    public LevelData GetLevelData(int levelID)
    {
        foreach (LevelData levelData in levelsData)
        {
            if (levelData.levelID == levelID)
            {
                return levelData;
            }
        }

        LevelData newLevelData = new LevelData(levelID);
        levelsData.Add(newLevelData);

        return newLevelData;
    }

    public void SetLevelData(int levelID, List<ObjectData> objectsData)
    {
        if (levelsData.Any(data => data.levelID == levelID))
        {
            LevelData newLevelData = levelsData.Find(data => data.levelID == levelID);

            levelsData.Remove(newLevelData);

            newLevelData.UpdateObjects(objectsData);

            levelsData.Add(newLevelData);
        }
        else
        {
            LevelData newLevelData = new LevelData(levelID, objectsData);
            levelsData.Add(newLevelData);
        }
    }

    public void ClearLevelData()
    {
        levelsData.Clear();
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        LoadFirstScene();
    }

    private void LoadFirstScene()
    {
        SceneLoader.Instance.LoadNewScene("Level1");
    }
}
