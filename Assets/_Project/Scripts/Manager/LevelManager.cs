﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance
    {
        get
        {
            if (_instance is null)
            {
                Debug.LogError("Level Manager is missing");

                return null;
            }
            return _instance;
        }
    }

    public event Action OnBeforeChangeScene;

    [SerializeField] private LevelData levelData;
    [SerializeField] private AudioClip levelSound = null;
    private static LevelManager _instance;

    public void BeforeChangeScene()
    {
        Fade.current.FadeIn(1);
        OnBeforeChangeScene?.Invoke();
    }

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        Fade.current.FadeOut(1);
        AudioManager.Instance.PlayBGMLoop(levelSound);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        OnBeforeChangeScene += SyncObjects;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        OnBeforeChangeScene -= SyncObjects;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        levelData = GameManager.Instance.GetLevelData(scene.buildIndex);

        if (levelData.saveObjects == null)
        {
            return;
        }

        if (levelData.saveObjects.Count < 1)
        {
            return;
        }

        foreach (ISaveObject saveObject in FindObjectsOfType<MonoBehaviour>().OfType<ISaveObject>().ToList())
        {
            foreach (ObjectData objectData in levelData.saveObjects.Where(objectData => saveObject.ID == objectData.ID))
            {
                saveObject.Position = objectData.position;
                saveObject.Active = objectData.active;
            }
        }
    }

    private void SyncObjects()
    {
        List<ObjectData> objectsData = new List<ObjectData>();

        foreach (ISaveObject saveObject in FindObjectsOfType<MonoBehaviour>().OfType<ISaveObject>().ToList())
        {
            ObjectData objectData = new ObjectData(saveObject.ID, saveObject.Position, saveObject.Active);
            objectsData.Add(objectData);
        }

        GameManager.Instance.SetLevelData(SceneManager.GetActiveScene().buildIndex, objectsData);
    }
}

[Serializable]
public struct LevelData
{
    public int levelID;
    public List<ObjectData> saveObjects;

    public LevelData(int newLevelID, List<ObjectData> newObjectsData = null)
    {
        levelID = newLevelID;
        saveObjects = newObjectsData;
    }

    public void UpdateObjects(List<ObjectData> newObjectsData)
    {
        saveObjects = newObjectsData;
    }
}

[Serializable]
public class ObjectData
{
    public int ID;
    public Vector3 position;
    public bool active;

    public ObjectData(int newID, Vector3 newPosition, bool newActive)
    {
        ID = newID;
        position = newPosition;
        active = newActive;
    }
}
