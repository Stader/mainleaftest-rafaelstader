﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtFreeCamera : MonoBehaviour
{
    private void Start()
    {
        CinemachineFreeLook freeLookCamera = FindObjectOfType<CinemachineFreeLook>();

        if (freeLookCamera != null)
        {
            freeLookCamera.LookAt = transform;
            freeLookCamera.Follow = transform;
        }
    }
}
