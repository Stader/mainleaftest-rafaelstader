﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNextCamera : MonoBehaviour
{
    [SerializeField] private GameObject collider;
    private bool triggerActivate = false;

    private void OnTriggerEnter(Collider other)
    {
        if (triggerActivate)
        {
            return;
        }

        if (!other.TryGetComponent<Player>(out Player player))
        {
            return;
        }

        triggerActivate = true;
        collider.SetActive(true);
        CamPathController.Current.ChangeCamera();
    }
}
