﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPathController : MonoBehaviour
{
    public static CamPathController Current;

    [SerializeField]
    private CinemachineBrain brain = null;
    [SerializeField]
    private CinemachineVirtualCamera vCam1;
    [SerializeField]
    private CinemachineVirtualCamera vCam2;
    [SerializeField]
    private CameraPathData[] cameraTargets = null;

    private int _currentCameraIndex = -1;
    private CinemachineTrackedDolly _trackedDollyVCam1;
    private CinemachineTrackedDolly _trackedDollyVCam2;

    public event Action OnChangeCamera;

    public void ChangeCamera()
    {
        OnChangeCamera?.Invoke();
    }

    private void Awake()
    {
        Initialize();
    }

    private void OnEnable()
    {
        OnChangeCamera += NextCamera;
    }

    private void OnDisable()
    {
        OnChangeCamera -= NextCamera;
    }

    private void Initialize()
    {
        Current = this;

        _trackedDollyVCam1 = vCam1.GetCinemachineComponent<CinemachineTrackedDolly>();
        _trackedDollyVCam2 = vCam2.GetCinemachineComponent<CinemachineTrackedDolly>();

        NextCamera();
    }

    private void NextCamera()
    {
        _currentCameraIndex++;

        if (_currentCameraIndex % 2 == 1)
        {
            vCam1.m_Priority = 0;
            vCam2.m_Priority = 20;
            StartCoroutine(RequestNewTarget(vCam1, _trackedDollyVCam1));
        }
        else
        {
            vCam1.m_Priority = 20;
            vCam2.m_Priority = 0;
            StartCoroutine(RequestNewTarget(vCam2, _trackedDollyVCam2));
        }
    }

    private IEnumerator RequestNewTarget(CinemachineVirtualCamera vCam, CinemachineTrackedDolly trackedDollyVCam)
    {
        yield return new WaitForSeconds(brain.m_DefaultBlend.m_Time);

        if (_currentCameraIndex + 1 < cameraTargets.Length)
        {
            ChangeNewTarget(vCam, trackedDollyVCam, _currentCameraIndex + 1);
        }
        else
        {
            ChangeNewTarget(vCam, trackedDollyVCam, _currentCameraIndex);
        }
    }

    private void ChangeNewTarget(CinemachineVirtualCamera vCam, CinemachineTrackedDolly trackedDollyVCam, int index)
    {
        CameraPathData cameraData = cameraTargets[index];

        trackedDollyVCam.m_Path = cameraData.smoothPath;
        trackedDollyVCam.m_PathOffset = cameraData.targetOffSet;
        vCam.m_LookAt = cameraData.targetTransform;

        if (cameraData.cameraPosition != Vector3.zero)
        {
            vCam.transform.position = cameraData.cameraPosition;
        }
    }
}

[Serializable]
public sealed class CameraPathData
{
    public CinemachineSmoothPath smoothPath;
    public Transform targetTransform;
    public Vector3 targetOffSet;
    public Vector3 cameraPosition;
}
