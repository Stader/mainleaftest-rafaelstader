﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    public static Action<int> OnScorePoints;

    [SerializeField] private TextMeshProUGUI actionText;
    [SerializeField] private TextMeshProUGUI scoreText;

    private void Awake()
    {
        UpdateScore(0);
    }

    private void OnEnable()
    {
        PlayerPhysics.OnCanHook += EnableCanHookUI;
        PlayerPhysics.OnRemoveCanHook += DisableCanHookUI;
        OnScorePoints += UpdateScore;
    }

    private void OnDisable()
    {
        PlayerPhysics.OnCanHook -= EnableCanHookUI;
        PlayerPhysics.OnRemoveCanHook -= DisableCanHookUI;
        OnScorePoints -= UpdateScore;
    }

    private void EnableCanHookUI()
    {
        actionText.text = "Grab";
    }

    private void DisableCanHookUI()
    {
        actionText.text = "";
    }

    private void UpdateScore(int newPoints)
    {
        int points = GameManager.Instance.Score;

        points += newPoints;

        scoreText.text = points.ToString("000");

        GameManager.Instance.Score = points;
    }
}
