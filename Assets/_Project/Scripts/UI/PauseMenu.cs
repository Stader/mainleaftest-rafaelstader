﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static Action Pause;
    public static Action UnPause;

    [SerializeField] private GameObject pausePanel = null;

    private PlayerInput _playerInput = null;

    public void UnPauseBtn()
    {
        UnPause?.Invoke();
    }

    public void RestartBtn()
    {
        GameManager.Instance.ClearLevelData();
        SceneLoader.Instance.LoadNewScene("Level1");
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    private void OnEnable()
    {
        Pause += OnPause;
        UnPause += OnUnPause;
    }

    private void OnDisable()
    {
        Pause -= OnPause;
        UnPause -= OnUnPause;
    }

    private void OnPause()
    {
        pausePanel.SetActive(true);

        if (_playerInput == null)
        {
            _playerInput = FindObjectOfType<PlayerInput>();
        }

        _playerInput.gamePaused = true;
    }

    private void OnUnPause()
    {
        pausePanel.SetActive(false);

        if (_playerInput == null)
        {
            _playerInput = FindObjectOfType<PlayerInput>();
        }

        _playerInput.gamePaused = false;
    }
}
