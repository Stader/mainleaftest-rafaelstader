﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    [SerializeField] private Image image;

    public static Fade current;

    public void FadeIn(float time)
    {
        StartCoroutine(FadeImage(true, time));
    }

    public void FadeOut(float time)
    {
        StartCoroutine(FadeImage(false, time));
    }

    private void Awake()
    {
        current = this;
    }

    private IEnumerator FadeImage(bool fadeIn, float time)
    {
        if (!fadeIn)
        {
            for (float i = time; i >= 0; i -= Time.deltaTime)
            {
                float alpha = i / time;
                image.color = new Color(0, 0, 0, alpha);

                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            for (float i = 0; i <= time; i += Time.deltaTime)
            {
                float alpha = i / time;
                image.color = new Color(0,0,0, alpha);

                yield return new WaitForEndOfFrame();
            }
        }
    }
}
