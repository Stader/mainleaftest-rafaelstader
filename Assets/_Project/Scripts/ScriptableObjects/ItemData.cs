﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(order = 0, fileName = "New Item Data", menuName = "Items/Item Data")]
public class ItemData : ScriptableObject
{
    public Color itemColor;
    public Color itemEmissionColor;
    public int points;
}
