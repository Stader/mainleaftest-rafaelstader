﻿using UnityEngine;

[ExecuteInEditMode()]
public sealed class Crystal : Item, ISaveObject
{
    public int ID => id;
    public Vector3 Position
    {
        get => transform.position;
        set => transform.position = value;
    }

    public bool Active
    {
        get => gameObject.transform.GetChild(0).gameObject.activeInHierarchy;
        set => gameObject.transform.GetChild(0).gameObject.SetActive(value);
    }

    [SerializeField] private int id = 0;
    [SerializeField] private float speedRotate = 30;
    [SerializeField] private Material crystalMaterial = null;
    [SerializeField] private AudioClip pickupClip = null;

    private void OnSkin()
    {
        crystalMaterial.color = data.itemColor;
        crystalMaterial.SetColor("_EmissionColor", data.itemEmissionColor * Mathf.LinearToGammaSpace(3f));
        gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material = crystalMaterial;
    }

    private void Awake()
    {
        OnSkin();
    }

    private void Update()
    {
        if (Application.isEditor)
        {
            OnSkin();
        }

        transform.Rotate(Vector3.forward * speedRotate * Time.deltaTime, Space.Self);
    }

    public override void ScorePoints()
    {
        if (!Active)
        {
            return;
        }
        PlayerUI.OnScorePoints?.Invoke(data.points);
        Active = false;
        AudioManager.Instance.TriggerSfx(pickupClip);
    }
}
