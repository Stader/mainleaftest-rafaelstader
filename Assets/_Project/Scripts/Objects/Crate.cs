﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : PullObject, ISaveObject
{
    public int ID => id;
    public Vector3 Position
    {
        get => transform.position;
        set => transform.position = value;
    }

    public bool Active
    {
        get => gameObject.transform.GetChild(0).gameObject.activeInHierarchy;
        set => gameObject.transform.GetChild(0).gameObject.SetActive(value);
    }

    [SerializeField] private float breakDistance;
    [SerializeField] private int id;

    public override void OnHook(Transform target)
    {
        hookedObject = target;
        transform.parent = target;
        hooked = true;
    }

    public override void RemoveHook()
    {
        hookedObject = null;
        transform.parent = null;
        hooked = false;
    }

    private void Update()
    {
        if (hooked)
        {
            CheckIfConnectionBreak();
        }
    }

    private void CheckIfConnectionBreak()
    {
        if (Vector3.Distance(hookedObject.position, transform.position) > breakDistance)
        {
            Debug.Log("A joint has just been broken!, distance: " + Vector3.Distance(hookedObject.position, transform.position));
            OnBreak();
        }
    }

    private void OnBreak()
    {
        hookedObject.GetComponent<PlayerPhysics>().Action();
    }
}
