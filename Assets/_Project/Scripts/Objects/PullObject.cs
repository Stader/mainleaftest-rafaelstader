﻿using UnityEngine;

public abstract class PullObject : MonoBehaviour
{
    [SerializeField] protected Rigidbody _rigidbody;
    [SerializeField] protected bool hooked;

    protected Transform hookedObject;

    public abstract void OnHook(Transform target);
    public abstract void RemoveHook();

}
