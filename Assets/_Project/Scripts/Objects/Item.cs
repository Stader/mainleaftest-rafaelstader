﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class Item : MonoBehaviour, ICanScore
{
    [SerializeField] protected ItemData data;
    public abstract void ScorePoints();

    protected void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();

        if (player == null)
        {
            return;
        }

        ScorePoints();
    }
}
