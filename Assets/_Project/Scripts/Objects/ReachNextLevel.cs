﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReachNextLevel : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();

        if (player == null)
        {
            return;
        }

        NextLevelAsync();
    }

    private async void NextLevelAsync()
    {
        LevelManager.Instance.BeforeChangeScene();

        CinemachineFreeLook freeLookCamera = FindObjectOfType<CinemachineFreeLook>();

        if (freeLookCamera != null)
        {
            freeLookCamera.LookAt = null;
            freeLookCamera.Follow = null;
        }

        await Task.Delay(1000);

        SceneLoader.Instance.LoadNewScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
