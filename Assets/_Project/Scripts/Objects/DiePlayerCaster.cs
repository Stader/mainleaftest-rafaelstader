﻿using UnityEngine;

public class DiePlayerCaster : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();

        if (player == null)
        {
            return;
        }

        player.Die();
    }
}
