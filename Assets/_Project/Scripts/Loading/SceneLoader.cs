using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader Instance;

    public string NextSceneString { get; private set; }

    public int NextSceneInt { get; private set; }

    public void LoadNewScene(string sceneName)
    {
        NextSceneString = sceneName;
        NextSceneInt = 0;
        SceneManager.LoadSceneAsync("Loading");
    }

    public void LoadNewScene(int sceneIndex)
    {
        NextSceneString = null;
        NextSceneInt = sceneIndex;
        SceneManager.LoadSceneAsync("Loading");
    }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
        {
            Debug.LogWarning("SceneLoader Destroy");
            Destroy(gameObject);
        }
    }
}
