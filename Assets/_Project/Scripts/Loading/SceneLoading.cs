using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoading : MonoBehaviour
{
    [SerializeField] private Image progressBarLoading;

    private void Start()
    {
        Time.timeScale = 1f;
        StartCoroutine(LoadAsyncOperation());
    }

    private IEnumerator LoadAsyncOperation()
    {
        AsyncOperation loadingLevel = SceneLoader.Instance.NextSceneString != null
                                          ? SceneManager.LoadSceneAsync(SceneLoader.Instance.NextSceneString)
                                          : SceneManager.LoadSceneAsync(SceneLoader.Instance.NextSceneInt);

        while (loadingLevel.progress < 1)
        {
            progressBarLoading.fillAmount = loadingLevel.progress;

            yield return new WaitForEndOfFrame();
        }
    }
}
