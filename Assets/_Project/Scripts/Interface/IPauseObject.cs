﻿public interface IPauseObject
{
    void Pause();

    void UnPause();
}
