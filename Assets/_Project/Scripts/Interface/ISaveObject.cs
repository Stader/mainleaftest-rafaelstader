﻿using UnityEngine;

public interface ISaveObject
{
    int ID { get; }
    Vector3 Position { get; set;  }
    bool Active { get; set; }
}
